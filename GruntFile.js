module.exports = function(grunt) {
    var globalConfig = {};
    grunt.log.header = function() {}
    grunt.fail.report = function() {
        if (grunt.fail.warncount > 0) {} 
        else {}
    };
    grunt.log.writeln = function () {}
    grunt.initConfig({
        globalConfig: globalConfig,

        emailBuilder:{
            litmus: {
                files: { '<%= globalConfig.folder %>/index-prod.html' : '<%= globalConfig.folder %>/index.html' },
                options: {
                    encodeSpecialChars: true,
                    applyWidthAttributes: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-email-builder');

    grunt.registerTask('build', 'Runs a task on a specified folder', function (folderName) {
        globalConfig.folder = folderName;
        if (folderName == null) {
            grunt.warn('Folder must be specified');
        }
        grunt.task.run('emailBuilder');
    });
};

