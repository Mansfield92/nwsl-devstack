'use strict';

const cf = {
    pug: "/pug/index.pug",
    sass: "/sass/style.sass",
    grunt: './GruntFile.js',
    rootDir: './build/',
    globalFolder: '',
    watchDir: 'build/**/',
};

const argv = require('yargs').argv;
const gulp = require('gulp');
const grunt = require('grunt');
const sass = require('gulp-ruby-sass');
const browserSync = require('browser-sync').create();
const pug = require('gulp-pug');
const runSequence = require('run-sequence');
const colors = require('colors/safe');
const fs = require('fs');
const path = require('path')
let gutil = require('gulp-util');

// Disable logging
gutil.log = function () {};

// Build SASS to CSS [sass/style.sass => style.css]
gulp.task('sass', function () {
    return sass(cf.globalFolder + cf.sass).pipe(gulp.dest(cf.globalFolder + '/'));
});

// Run task SASS, then inline CSS to HTML
gulp.task('sassHTML', ['sass'], function () {
    runSequence('HTML', function () {});
});

// Build PUG to HTML [pug/index.pug => index.html]
gulp.task('pug', function () {
    return gulp.src(cf.globalFolder + cf.pug)
        .pipe(pug({pretty: true}))
        .pipe(gulp.dest(cf.globalFolder + '/'))
});

// Run task PUG then task HTML
gulp.task('pugHTML', ['pug'], function () {
    runSequence('HTML', function () {
        log('PUG + HTML');
    })
});

// GRUNT Task to inline CSS to HTML and to convert special characters to HTML entities [index.html + style.css => index-prod.html]
gulp.task('HTML', function () {
    return grunt.tasks(
        ['build:' + cf.globalFolder],
        {gruntfile: cf.grunt}, function (e) {
            browserSync.reload({stream: true});
        }
    );
});

// Run task pug followed by task sassHTML
gulp.task('buildAll', ['pug'], function () {
    runSequence('sassHTML', function (e) {
        log('PUG + SASS + HTML');
    });
});

// Set argument called "f" as globalFolder 
gulp.task('b', function () {
    cf.globalFolder = (argv.f.indexOf('build/') == -1) ? ('build/' + argv.f) : argv.f;
    cf.globalFolder.substr(-1) == '/' && (cf.globalFolder = cf.globalFolder.substr(0, cf.globalFolder.length - 1));
    gulp.start('buildAll');
});

// Build SASS and HTML in folder with --parameter
gulp.task('build', ['b'], function () {});

// Default task to Develop Newsletter - Watching all folders in /build to build SASS or PUG
// Browser sync to create /build/index.html from folders in /build 
gulp.task('default', function () {
    createIndex();

    browserSync.init({
        server: cf.rootDir,
        port: 7001,
        logFileChanges: false,
        ui: {
            port: 7002
        }
    },function (err, bs) {
        console.log(colors.gray('--------------------------------------------------------------------'));
        console.log(colors.blue.bgYellow('************************ Server has started ************************'));
        console.log(colors.gray('--------------------------------------------------------------------'));
    });

    gulp.watch(cf.rootDir + "**/index-prod.html").on('change', browserSync.reload);

    gulp.watch(
        [
            (cf.watchDir + '*.sass'),
            (cf.watchDir + '*.scss'),
            (cf.watchDir + '*.pug')
        ], function (file) {

            let path = file.path;
            let isSass = (path.indexOf('.sass') != -1 || path.indexOf('.scss') != -1);

            if (isSass) {
                path = path.substr(0, path.indexOf('/sass'));
                path = path.substr(path.lastIndexOf('/') + 1, path.length);
            } else {
                path = path.substr(0, path.indexOf('/pug'));
                path = path.substr(path.lastIndexOf('/') + 1, path.length);
            }

            cf.globalFolder = 'build/' + path;

            if (isSass) {
                runSequence('sassHTML', function (e) {
                    log('SASS + HTML', true);
                });
            } else {
                runSequence('pugHTML');
            }
        });
});

const log = (m, red) => {
    if (red === true) {
        console.log(colors.red(time()) + colors.green(' TASK FINISHED:  ' + colors.red(m + ' [' + (cf.globalFolder.substr(cf.globalFolder.indexOf('/') + 1)) + ']')));
    } else if (red === false) {
        console.log(colors.greem(m));
    } else {
        console.log(colors.red(time()) + colors.green(' TASK FINISHED:  ' + colors.blue(m + ' [' + (cf.globalFolder.substr(cf.globalFolder.indexOf('/') + 1)) + ']')));
    }
    console.log(colors.gray('-------------------------------------------------------------------'));
}

const time = () => {
    let date = new Date();
    let hh = date.getHours();
    let mm = date.getMinutes();
    let ss = date.getSeconds();
    return ('[' + (hh < 10 ? ('0' + hh) : hh) + ":" + (mm < 10 ? ('0' + mm) : mm) + ":" + (ss < 10 ? ('0' + ss) : ss) + ']');
};


function getDirectories (srcpath) {
    return fs.readdirSync(srcpath).filter(file => fs.statSync(path.join(srcpath, file)).isDirectory());
}

function pugHTML(body) {
    return '<!DOCTYPE html><html><body>' + body + '</body></html>';
}

function createIndex() {
    const files = getDirectories(cf.rootDir);
    let links = "";

    for( let i = 0; i < files.length; i++){
        if(files[i] != 'shared') {
            // links += "<div><a href='" + files[i] + "/index.html'>" + files[i] + "</a></div>";
            links += "<div><a href='" + files[i] + "/index-prod.html'>PROD: " + files[i] + " </a></div>";
            links += "<hr>";
        }
    }

    let stream = fs.createWriteStream(cf.rootDir + 'index.html');

    stream.once('open', function(fd) {
        var html = pugHTML(links);
        stream.end(html);
    });
}