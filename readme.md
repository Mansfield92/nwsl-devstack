# NWSL devstack

## Version
1.0.0

## Description
A simple newsletter devstack to make your life easier.

## Installation
```sh
$ git clone https://Mansfield92@bitbucket.org/Mansfield92/nwsl-devstack.git
$ npm i
```

## Development
 - You can use npm script with default --silent settings (recommended)
```sh
$ npm run dev
```
 - or you can just simply use gulp
```sh
$ gulp
```
 - or gulp with silent option
```sh
$ gulp --silent
```

## Production
```sh
$ npm run build build/example_nwsl/
```
- or with gulp
```sh
$ gulp b --f build/161213-Transfer-pl/
```

You can write filename in 4 possible ways:

- build/example_nwsl/
- build/example_nwsl
- example_nwsl/
- example_nwsl

**If you haven't changed the newsletter after stopping development, you don't need to build it.**

## Usage

### **Before starting development:**
- All your newsletter projects must be located in **/build** directory.
- In you newsletter folder, there must be a **/sass/style.sass** file and
  **/pug/index.pug** file.
- Just copy **"example_nwsl"** project and develop. 

### **After starting development:**
- Click on link with your newsletter folder name.
- You are now looking at index-prod.html file with inlined CSS and with html entities instead of special characters.
- Now you can simply write in your SASS or PUG files and it will quickly update your index-prod.html file and reload your browser.
- If you create new SASS or PUG file, you don't need to restart development.
- **If you delete SASS or PUG file, which is imported somewhere => it will crash**.


## How it works?
### **If you chage SASS file:**
- /sass/style.sass is compiled to style.css in project, where you changed the SASS file.
- index-prod.html is created with inlined style.css file from previous step.
- Browser window is reloaded and you can see your changes.

### **If you chage PUG file:**
- /pug/index.pug is rendered to index.html in project, where you changed the PUG file.
- index-prod.html is created with inlined style.css.
- Browser window is reloaded and you can see your changes.